#include "AI.h"
#include<vector>
int TIE_VAL = -1;
int NO_VAL = 0;
int X_VAL = 1;
int O_VAL = 2;

void AI::init(Player& aiPlayer)
{
	_aiPlayer = aiPlayer;
	//if (_aiPlayer == X_VAL)
	if(_aiPlayer.getSimbol() == 'X')
	{
		//_humanPlayer = O_VAL;
		_humanPlayer.setSimbol('O');
	}
	else {
		//_humanPlayer = X_VAL;
		_humanPlayer.setSimbol('X');
	}
}

void AI::performMove(Board& board)
{
	AiMove bestMove = getBestMove(board, _aiPlayer);
	
	//board.setVal(bestMove.x, bestMove.y, _aiPlayer);
	Board::Position position = { static_cast<uint8_t>(bestMove.x), static_cast<uint8_t>(bestMove.y) };

}
// functie sa puna singur o piesa pe tabla de joc
Board::Position AI::performAiMove( Piece && piece, Player& player)
{
	Board board;

	AiMove bestMove = getBestMove(board, _aiPlayer);


	Board::Position position = { static_cast<uint8_t>(bestMove.x), static_cast<uint8_t>(bestMove.y) };

	auto& optionalPiece = board[position]; // o variabila auxiliara care retine pozitia

	optionalPiece = std::move(piece);
	
	return position;

}
// am folosit un algoritm minMax pentru calcularea celei mai bune pozitii
AiMove AI::getBestMove(Board& board, Player& player)
{
	// Base state, check for end state
	//Player rv = static_cast<Player>(BoardStateChecker::State::Win);
	Player rv;

	if (rv == _aiPlayer)
	{
		return AiMove(10);
	}
	else if (rv == _humanPlayer)
	{
		return AiMove(10);
	}
	//else if (rv == BoardStateChecker::State::None)
	//{
	//	return AiMove(0);
	//}
	// am adaugat in vector mutarile
	std::vector<AiMove> moves;

	// Do the recursive function calls and construct the moves vector
	for (int y = 0; y < 5; y++)
	{
		for (int x = 0; x < 5; x++)
		{
				AiMove move;
				move.x = x;
				move.y = y;
			//	board.setVal(x, y, player);
				Board::Position position = { static_cast<uint8_t>(x), static_cast<uint8_t>(y) };
				if (player == _aiPlayer)
				{
					move.score = getBestMove(board, _humanPlayer).score; // conteaza doar scorul
				}
				else
				{
					move.score = getBestMove(board, _aiPlayer).score;

				}

				moves.push_back(move);
			//	board.setVal(x, y, NO_VAL);
		}
	}
	// Pick the best move for the current player
	int bestMove = 0;
	// ia cel mai mare scor din vector
	if (player == _aiPlayer)
	{
		int bestScore = -100000;
		for (int i = 0; i < 4; i++)
		{
			if (moves[i].score > bestScore)
			{
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}
	// ia cel mai mic scor din vector
	else
	{
		int bestScore = 100000;
		for (int i = 0; i < 4; i++)
		{
			if (moves[i].score < bestScore)
			{
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}
	// Return the best move
	return moves[bestMove];
}