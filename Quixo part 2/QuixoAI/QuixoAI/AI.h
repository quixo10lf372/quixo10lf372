#pragma once
#include"Board.h"
#include"Player.h"
#include"BoardStateChecker.h"

// am folosit un struct pentru impelmentarea scorului
struct AiMove {
	AiMove() {};
	AiMove(int Score) : score(Score) {};
	unsigned x;
	unsigned y;
	int score;
};



class AI
{
public:

	// Initializeaza AI player
	void init(Player& aiPlayer);
	// Performs the AI move
	void performMove(Board& board);
	Board::Position performAiMove( Piece&& piece, Player& player);

private:
	// calculeaza cea mai buna pozitie pentru a pune piesa de tabla de joc
	AiMove getBestMove(Board& board, Player& player );

	Player _aiPlayer; // indexul AI player
	Player _humanPlayer; // indexul player-ului
};

