#include "Board.h"
#include<cassert>
#include<tuple>
#include<algorithm>
#include <windows.h>

const int BEGIN_LINE = 0;
const int END_LINE = 4;
const int BEGIN_COLUMN = 0;
const int END_COLUMN = 4;

// apeleaza constructorul fara parametrii al piesei
const std::optional<Piece> Board::base_iterator::kInvalidPiece = Piece();
const char kEmptyBoardCell[] = "_";

// implentarea iteratorului
Board::base_iterator::base_iterator(std::array<std::optional<Piece>, kSize>& data, size_t offset) :
	m_data(data), m_offset(offset)
{

}
// implementarea operatorului *
const std::optional<Piece>& Board::base_iterator::operator*()
{
	assert(m_offset < kSize);
	return m_data[m_offset].has_value() ? m_data[m_offset] : kInvalidPiece;
}
// implementarea operatorului ->
const std::optional<Piece>* Board::base_iterator::operator-> ()
{
	return &**this;
}

// implementarea operatorului !=
bool Board::base_iterator::operator!=(const base_iterator& other)
{
	return m_offset != other.m_offset;
}

// implementarea operatorului ++
Board::line_iterator& Board::line_iterator::operator++()
{
	++m_offset;
	return *this;
}

Board::line_iterator& Board::line_iterator::operator++(int)
{
	return ++(*this);
}

Board::column_iterator& Board::column_iterator::operator++()
{
	m_offset = m_offset + kWidth;
	return *this;
}

Board::column_iterator& Board::column_iterator::operator++(int)
{
	return ++(*this);
}

Board::main_diagonal_iterator& Board::main_diagonal_iterator::operator++()
{
	m_offset = m_offset + kWidth + 1;
	return *this;
}

Board::main_diagonal_iterator& Board::main_diagonal_iterator::operator++(int)
{
	return ++(*this);
}

Board::secondary_diagonal_itertor Board::secondary_diagonal_itertor::operator++()
{
	m_offset = m_offset + kWidth - 1;
	return *this;
}

Board::secondary_diagonal_itertor Board::secondary_diagonal_itertor::operator++(int)
{
	return ++(*this);
}

// Getter
const std::optional<Piece>& Board::operator[] (const Position& position) const
{
	// despacheteaza position ( referinte constante catre first si catre last)
	const auto&[line, coloumn] = position;

	if (line >= kHeight || coloumn >= kWidth)
		throw "Board index out of bounds";

	return m_pieces[line * kWidth + coloumn];
}

// poate fi si Getter si Setter
std::optional<Piece>& Board::operator[] (Position& position)
{
	auto &[line, coloumn] = position;
	if (line >= kHeight || coloumn >= kWidth)
		throw "Board index out of bounds";

	// nu este posibil ca un player sa puna direct o piesa in centrul tablei
	/*if (line >= kHeight || coloumn >= kWidth
		|| line == 1 && coloumn == 1 || line == 1 && coloumn == 2 || line == 1 && coloumn == 3
		|| line == 2 && coloumn == 1 || line == 2 && coloumn == 2 || line == 2 && coloumn == 3
		|| line == 3 && coloumn == 1 || line == 3 && coloumn == 2 || line == 3 && coloumn == 3)
		throw "Board index out of bounds";*/
	//if (line == coloumn)
	//{
	//	ShiftLine(position);
	//}

	return m_pieces[line * kWidth + coloumn];
}


// functie pentru shiftarea coloanei
std::optional<Piece>& Board::ShiftColumn(Position & position)
{
	auto &[line, coloumn] = position;
	if (coloumn = 4)
	{
		for (; line > 0; line--)
		{
			line = line - 1;
		}
	}
	else
	{
		for (; line < 4; line++)
		{
			line = line + 1;
		}
	}
	return m_pieces[line * kWidth + coloumn];
}


// functie pentru shift-area liniei
std::optional<Piece>& Board::ShiftLine(Position & position)
{
	auto &[line, coloumn] = position;
	if (line == 4)
	{
		for (; coloumn > 0; coloumn--)
		{
			coloumn = coloumn - 1;
		}
	}
	else
	{
		for (; coloumn < 4; coloumn++)
		{
			coloumn = coloumn + 1;
		}
	}
	return m_pieces[line * kWidth + coloumn];
}

// getter pentru iteratorul de linie
std::pair<Board::line_iterator, Board::line_iterator> Board::GetLine(uint32_t index)
{
	return std::make_pair(line_iterator(m_pieces, index* kWidth), line_iterator(m_pieces, (index + 1)* kWidth));
}

// getter pentru iteratorul de coloana
std::pair<Board::column_iterator, Board::column_iterator> Board::GetColumn(uint32_t index)
{
	return std::make_pair(column_iterator(m_pieces, index), column_iterator(m_pieces, kSize + index));
}
// getter pentru iteratorul de diagonala principala
std::pair<Board::main_diagonal_iterator, Board::main_diagonal_iterator> Board::GetMainDiagonal()
{
	return std::make_pair(main_diagonal_iterator(m_pieces, 0), main_diagonal_iterator(m_pieces, kSize + kWidth));
}
// getter pentru iteratorul de diagonala secundara
std::pair<Board::secondary_diagonal_itertor, Board::secondary_diagonal_itertor> Board::GetSecondaryDiagonal()
{
	return std::make_pair(secondary_diagonal_itertor(m_pieces, kWidth - 1), secondary_diagonal_itertor(m_pieces, kSize - 1));
}
// un boolean care verifica daca tabla de joc este plina
bool Board::isFull() const
{
	return std::all_of(
		m_pieces.begin(),
		m_pieces.end(),
		[](const std::optional<Piece>& optionalPiece)
	{
		return optionalPiece.has_value();
	}
	);
}
// functie de mutare in sus a piesei
void Board::moveUpper(Board::Position position)
{
	Board board;
	auto choicenPiece = board[position];
	auto &[line, column] = position;
	Piece piece;
	auto tempPieceType = piece.Piece::GetFace();
	for (int lineIndex = line; lineIndex > 0; --lineIndex)
	{
		auto tempPosition = std::make_pair(lineIndex, column);
		auto newPosition = std::make_pair(lineIndex - 1, column);
	}
	std::pair<uint8_t, uint8_t> newMovedPosition;
	newMovedPosition.first = BEGIN_LINE;
	newMovedPosition.second = column;
	std::swap(choicenPiece, board[newMovedPosition]);
}

// optiune catre mutare in jos a piesei
void Board::moveDown(Board::Position position)
{
	Board board;
	auto choicenPiece = board[position];
	auto[line, column] = position;
	Piece piece;
	auto tempPieceType = piece.Piece::GetFace();
	for (uint8_t lineIndex = line; lineIndex < 4; ++lineIndex)
	{
		std::pair<int, int> tempPosition = std::make_pair(lineIndex, column);
		std::pair<int, int> newPosition = std::make_pair(lineIndex + 1, column);
	}
	std::pair<uint8_t, uint8_t> newMovedPosition;
	newMovedPosition.first = END_LINE;
	newMovedPosition.second = column;
	std::swap(choicenPiece, board[newMovedPosition]);
}

// optiune catre mutare la stanga a piesei
void Board::moveLeft(Board::Position position)
{
	Board board;
	auto choicenPiece = board[position];
	auto[line, column] = position;
	Piece piece;
	auto tempPieceType = piece.Piece::GetFace();
	for (int columnIndex = column; columnIndex > 0; --columnIndex)
	{
		std::pair<int, int> tempPosition;
		tempPosition = std::make_pair(line, columnIndex);
		std::pair<int, int> newPosition;
		newPosition = std::make_pair(line, columnIndex - 1);
	}
	std::pair<uint8_t, uint8_t> newMovedPosition;
	newMovedPosition.first = line;
	newMovedPosition.second = BEGIN_COLUMN;
	std::swap(choicenPiece, board[newMovedPosition]);
}

// optiune pentru mutarea la dreapta a piesei
void Board::moveRight(Board::Position position)
{
	Board board;
	auto choicenPiece = board[position];
	auto[line, column] = position;
	Piece piece;
	auto tempPieceType = piece.Piece::GetFace();
	for (int columnIndex = 0; columnIndex < 4; ++columnIndex)
	{
		std::pair<int, int> tempPosition;
		tempPosition = std::make_pair(line, columnIndex);
		std::pair<int, int> newPosition;
		newPosition = std::make_pair(line, columnIndex + 1);
	}
	std::pair<uint8_t, uint8_t> newMovedPosition;
	newMovedPosition.first = line;
	newMovedPosition.second = END_COLUMN;
	std::swap(choicenPiece, board[newMovedPosition]);
}

// afisarea tablei de joc 
std::ostream& operator<<(std::ostream& os, const Board& board)
{
	Board::Position position;
	auto&[line, column] = position;

	for (line = 0; line < Board::kHeight; ++line)
	{
		for (column = 0; column < Board::kWidth; ++column)
		{
			if (board[position])
				os << *board[position];
			else
				os << kEmptyBoardCell;
			os << ' ';
		}
		os << std::endl;
	}
	return os;
}

