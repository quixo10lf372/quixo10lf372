#pragma once
#include "Board.h"
class BoardStateChecker
{
public:
	// cazurile in care se poate termina jocul
	enum class State
	{
		None,
		Win,
		Draw
	};

	// functie care verifica daca un player a castigat
	static State Check(const Board& board, const Board::Position& position);
};

