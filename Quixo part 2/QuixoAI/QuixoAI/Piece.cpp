#include "Piece.h"



// constructorul piesei
Piece::Piece()
{
	m_face = Face::None; // la inceput piesele sunt none
}


Piece::Piece(Face face) :
	m_face(face)
{
	// constructorul cu un parametru al piesei
}

// constructorul de copiere
Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::Piece(Piece&& other)
{
	*this = std::move(other);
}
// operatorul de egal
Piece& Piece::operator= (const Piece& other)
{
	m_face = other.m_face;
	return *this;
}

Piece& Piece::operator= (Piece&& other)
{
	m_face = other.m_face;
	new(&other) Piece;
	return *this;
}

Piece::~Piece()
{
	// destructorul piesei
}

// getter pentru fata piesei
Piece::Face Piece::GetFace() const
{
	return m_face;
}

// implementarea operatorului &=
Piece& Piece::operator&=(const Piece & other)
{
	if (this->m_face != other.m_face)
		this->m_face = Face::None;
	return *this;
}
// implementarea operatorului &
Piece Piece::operator&(Piece other) const
{
	other &= *this;
	return other;
}

// afisarea unei piese
std::ostream& operator<<(std::ostream& os, const Piece& piece)
{
	//switch (piece.m_face)
	//{
	//case Piece::Face::OFace: os << "O"; break;
	//case Piece::Face::XFace: os << "X"; break;
	//	//	case Piece::Face::None: os << "_"; break;
	//default:
	//	break;
	//}
	return os <<
		static_cast<int>(piece.m_face);
	//return os;
}