#pragma once
#include"Board.h"

#include<string>

class Player
{
public:
	// constructorul fara parametrii ai unui player
	Player();
	// constructorul cu parametrii care primeste numele player-ului si ce simbol foloseste (X/O)
	Player(const std::string& name, const char simbol);
	// afiseaza un player
	friend std::ostream& operator << (std::ostream& os, const Player& player);
	friend bool operator==(const Player&a, const Player& b);
	// setter pentru sinbolul pe care il foloseste un player
	void setSimbol(const char simbol);
	// getter pentru simbolul pe care il foloseste un player
	char getSimbol() const;
	// getter pentru numele jucatorului
	std::string getName() const;

	// metoda pentru a pune o piesa pe tabla de joc
	Board::Position PlacePiece(std::istream& in, Piece&& piece, Board& board) const;
	// metoda pentru a muta o piesa pe tabla de joc
	Board::Position MovePiece(std::istream & in, Piece && piece, Board & board) const;

	// destructorul unui Player
	~Player();

public:
	std::string m_name;
	char m_simbol;
};

