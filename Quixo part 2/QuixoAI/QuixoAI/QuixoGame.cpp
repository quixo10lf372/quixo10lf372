#include "QuixoGame.h"


void QuixoGame::Run()
{
	// initializarile celor 2 jucatori

	std::string playerName;
	std::cout << "Player's name with X symbol: ";
	std::cin >> playerName;
	Player firstPlayer(playerName, 'X');

	std::cout << "Player's name with O symbol: ";
	std::cin >> playerName;
	Player secondPlayer(playerName, 'O');

	std::reference_wrapper<Player> XPlayer = firstPlayer;
	std::reference_wrapper<Player> OPlayer = secondPlayer;
//	int _aiPlayer = static_cast<int>(firstPlayer.getSimbol());	
	Player _aiPlayer("ai", 'O');
	std::reference_wrapper<Player> aiPlayer = _aiPlayer;


	// initializarea tablei de joc
	Board board;

	// main loop
	while (true)
	{
		system("cls");

		std::cout << "Asa arata tabla de joc:\n";
		std::cout << board << std::endl;

		Piece pickedPiece = Piece::Face::OFace;
		Piece piece = Piece::Face::XFace;
		//	pickedPiece.GetFace() = Piece::Face::XFace;
		std::cout << XPlayer << "'s turn";
	//	std::cout << " " << pickedPiece;

		Board::Position placedPosition;
		AI ai;
		while (true)
		{
			try
			{
				if (XPlayer.get().getSimbol() == 'X')
				{
					std::swap(pickedPiece, piece);
				}
				placedPosition = XPlayer.get().PlacePiece(std::cin, std::move(pickedPiece), board);
			//	ai.init(_aiPlayer);
			//	placedPosition = XPlayer.get().MovePiece(std::cin, std::move(pickedPiece), board);
			//	placedPosition = ai.performAiMove(std::move(pickedPiece), _aiPlayer);
				break;
			}
			catch (const char* errorMessage)
			{
				std::cout << errorMessage << std::endl;
			}
		}
		// conditia de castig
		auto state = BoardStateChecker::Check(board, placedPosition);
		if (state == BoardStateChecker::State::Win)
		{
			std::cout << "Avem un castigator!\nFelicitari: " << XPlayer << std::endl;
		}
		else if (state == BoardStateChecker::State::Draw)
		{
			std::cout << "Joc egal" << std::endl;
		}

		// schimbarea turei celor 2 jucatori
		std::swap(XPlayer, aiPlayer);
	}
}
