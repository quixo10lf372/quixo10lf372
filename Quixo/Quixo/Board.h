#pragma once
#include"Piece.h"
#include<iostream>
#include<optional>
#include<array>


class Board
{
public:
	// dimensiunile tablei de joc
	static const size_t kWidth = 5;
	static const size_t kHeight = 5;
	static const size_t kSize = kHeight * kWidth;
public:
	// am retinut pozitile unei piese cu ajutorul lui pair
	using Position = std::pair<uint8_t, uint8_t>;

	// am folosit o clasa base_iterator pentru deplasarea pieselor pe tabla
	class base_iterator
	{
		static const std::optional<Piece> kInvalidPiece;

	public:
		base_iterator(std::array<std::optional<Piece>, kSize>& data, size_t offset);
		const std::optional<Piece>& operator *();
		const std::optional<Piece>* operator ->();
		bool operator !=(const base_iterator& other);

	public:
		std::array<std::optional<Piece>, kSize>& m_data;
		size_t m_offset;
	};
	class line_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;
		line_iterator& operator ++();
		line_iterator& operator ++(int);
	};

	class column_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		column_iterator& operator ++();
		column_iterator& operator ++(int);
	};

	class main_diagonal_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		main_diagonal_iterator& operator ++();
		main_diagonal_iterator& operator ++(int);

	};

	class secondary_diagonal_itertor : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		secondary_diagonal_itertor operator ++();
		secondary_diagonal_itertor operator ++(int);
	};

public:
	Board() = default;

	// Getter
	const std::optional<Piece>& operator[] ( const Position& position) const;

	// Getter si Setter
	std::optional<Piece>& operator[] ( Position& position);

	friend std::ostream& operator << (std::ostream& os, const Board& board);

	std::pair<line_iterator, line_iterator> GetLine(uint32_t index);
	std::pair<column_iterator, column_iterator> GetColumn(uint32_t index);
	std::pair<main_diagonal_iterator, main_diagonal_iterator> GetMainDiagonal();
	std::pair<secondary_diagonal_itertor, secondary_diagonal_itertor> GetSecondaryDiagonal();

	bool isFull() const; // verifica daca tabla este plina

	// shift-arile pieselor
	std::optional<Piece>& ShiftLine(Position& pos);
	std::optional<Piece>& ShiftColumn(Position& pos);

	// mutarile pieselor pe tabla de joc
	void moveUpper( Board::Position position);
	void moveDown( Board::Position position);
	void moveLeft( Board::Position position);
	void moveRight( Board::Position position);




public:
	int BEGIN_LINE = 0;
	int END_LINE = 4;
	int BEGIN_COLUMN = 0;
	int END_COLUMN = 4;


public:
	// am retinut piesele intr-un array
	std::array<std::optional<Piece>, kSize> m_pieces;

};

