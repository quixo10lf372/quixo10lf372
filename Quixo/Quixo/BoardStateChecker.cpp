#include "BoardStateChecker.h"
#include <numeric>

// metoda pentru a verifica daca un player a castigat
BoardStateChecker::State BoardStateChecker::Check(const Board& b, const Board::Position& position)
{
	Board& board = const_cast<Board&>(b);
	const auto&[lineIndex, columnIndex] = position;

	auto[first, last] = board.GetLine(lineIndex);
	auto reducer = [](const Piece& piece1, const std::optional<Piece> &piece2)
	{
		return piece1 & piece2.value();
	};
	Piece piece = std::accumulate(first, last, (first++)->value(), reducer);
	if (piece.GetFace() != Piece::Face::XFace ||
		piece.GetFace() != Piece::Face::OFace)
		return State::Win;

    return State::None;
}
