#include "Move.h"



Move::Move()
{
}

Move::Move(Board board, int srcX, int srcY, int destX, int destY)
	: board(board), srcX(srcX), srcY(srcY), destX(destX), destY(destY)
{
}

Board Move::getBoard()const
{
	return this->board;
}

Move::~Move()
{
}

/*int Move::leftInsert() {
	int row = selected_row;
	int col = selected_col;
	int tmp = state[row][col];
	int s = -1;
	for (int i = col; i >= 0; i--) {
		setState(row, i, (i == 0 ? tmp : state[row][i - 1]));
		if (s != -1) {
			continue;
		}
		s = check(row, i, true, false);
	}
	return s;
}

int Move::rightInsert() {
	int row = selected_row;
	int col = selected_col;
	int tmp = state[row][col];
	int s = -1;
	for (int i = col; i < SIZE; i++) {
		setState(row, i, (i == SIZE - 1 ? tmp : state[row][i + 1]));
		if (s != -1) {
			continue;
		}
		s = check(row, i, true, false);
	}
	return s;
}

int Move::downInsert() {
	int row = selected_row;
	int col = selected_col;
	int tmp = state[row][col];
	int s = -1;
	for (int i = row; i < SIZE; i++) {
		setState(i, col, (i == SIZE - 1 ? tmp : state[i + 1][col]));
		if (s != -1) {
			continue;
		}
		s = check(i, col, false, true);
	}
	return s;
}

int Move::topInsert() {
	int row = selected_row;
	int col = selected_col;
	int tmp = state[row][col];
	int s = -1;
	for (int i = row; i >= 0; i--) {
		setState(i, col, (i == 0 ? tmp : state[i - 1][col]));
		if (s != -1) {
			continue;
		}
		s = check(i, col, false, true);
	}
	return s;
}

int Move::checkHorizontal(int row, int col) {
	int s = state[row][0];
	for (int i = 0; i < SIZE; i++) {
		if (state[row][i] != s) {
			return -1;
		}
	}
	return (s == 2 ? -1 : s);
}

int Move::checkVertical(int row, int col) {
	int s = state[0][col];
	for (int i = 0; i < SIZE; i++) {
		if (state[i][col] != s) {
			return -1;
		}
	}
	return (s == 2 ? -1 : s);
}

int Move::checkBackSlash(int row, int col) {
	int s = state[0][0];
	for (int i = 0; i < SIZE; i++) {
		if (state[i][i] != s) {
			return -1;
		}
	}
	return (s == 2 ? -1 : s);
}

int Move::checkForwardSlash(int row, int col) {
	int s = state[0][SIZE - 1];
	for (int i = 0; i < SIZE; i++) {
		if (state[i][SIZE - i - 1] != s) {
			return -1;
		}
	}
	return (s == 2 ? -1 : s);
}
*/
