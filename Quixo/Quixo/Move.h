#pragma once
#include"Board.h"

class Move
{
public:
	const int selected_row = -1;
	const int selected_col = -1;
public:
	Move();
	Move(Board board, int srcX, int srcY, int destX, int destY);
	~Move();
	int leftInsert();


	int rightInsert();


	int downInsert();

	int topInsert();
	int checkHorizontal(int row, int col);

	int checkVertical(int row, int col);

	int checkBackSlash(int row, int col);

	int checkForwardSlash(int row, int col);
	Board getBoard() const;

private:
	int srcX;
	int srcY;
	int destX;
	int destY;
	Board board;
};

