#pragma once

#include <iostream>
#include <unordered_map>

class Piece
{
public:
	// am folosit un enum pentru fetele unei piese
	enum class Face
	{
		None,
		XFace,
		OFace
	};

public:
	// constructorul fara parametrii al unei piese
	Piece();
	// constructorul care ia ca parametru o fata a piesei
	Piece(Face face);
	Piece(const Piece &other);
	Piece(Piece && other);
	// destructorul
	~Piece();
	// Getter pentru fetele piesei
	Face GetFace() const;
	// implementarile operatororilor clasei
	Piece & operator = (const Piece & other);
	Piece & operator = (Piece && other);
	Piece & operator &= (const Piece & other);
	Piece operator & (Piece other) const;

	// afiseaza piesele pe tabla de joc
	friend std::ostream& operator << (std::ostream& os, const Piece& piece);


public:
	Face m_face : 1;
	std::unordered_map<std::string, Piece> m_pool; // am folosit un unordered map
};

