#include "Player.h"




Player::Player()
{
	//default Constructor
}


// constructorul cu parametrii care primeste numele jucatorului si ce simbol foloseste (X/O)
Player::Player(const std::string& name, const char simbol) :
	m_name(name), m_simbol(simbol) // initializare cu lista de parametrizare
{

}

// afiseaza un player
std::ostream& operator<<(std::ostream& os, const Player& player)
{
	return os << player.m_name;
}

// setter pentru sinbolul pe care il foloseste un player
void Player::setSimbol(char simbol)
{
	this->m_simbol = simbol;
}

// getter pentru simbolul pe care il foloseste un player
char Player::getSimbol() const
{
	return m_simbol;
}

// getter pentru numele jucatorului
std::string Player::getName()const
{
	return m_name;
}

// metoda pentru a pune o piesa pe tabla de joc
Board::Position Player::PlacePiece(std::istream & in, Piece && piece, Board & board) const
{
	uint16_t line = UINT16_MAX;
	uint16_t column = UINT16_MAX;

	if (in >> line)
		if (in >> column)
		{
			Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column) };

			auto& optionalPiece = board[position]; // o variabila auxiliara care retine pozitia

			optionalPiece = std::move(piece);

			return position;
		}

	in.clear();
	in.seekg(std::ios::end);

	throw "Please enter only two numbers from 0 to 4.";
}

// metoda pentru a muta o piesa pe tabla de joc
Board::Position Player::MovePiece(std::istream & in, Piece && piece, Board & board) const
{
	uint16_t line = UINT16_MAX;
	uint16_t column = UINT16_MAX;
	std::string up = "up";
	std::string down = "down";
	std::string left = "left";
	std::string right = "right";
	Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column) };

	if (in >> up)
	{
		board.moveUpper(position);
	}

	if (in >> down)
	{
		board.moveDown(position);
	}

	if (in >> right)
	{
		board.moveRight(position);
	}

	if (in >> left)
	{
		board.moveLeft(position);
	}

	return position;
}

Player::~Player()
{
	// default destructor
}
