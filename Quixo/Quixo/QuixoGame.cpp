#include "QuixoGame.h"


void QuixoGame::Run()
{
	// initializarile celor 2 jucatori

	std::string playerName;
	std::cout << "Player's name with X symbol: ";
	std::cin >> playerName;
	Player firstPlayer(playerName, 'X');

	std::cout << "Player's name with O symbol: ";
	std::cin >> playerName;
	Player secondPlayer(playerName, 'O');

	std::reference_wrapper<Player> XPlayer = firstPlayer;
	std::reference_wrapper<Player> OPlayer = secondPlayer;

	// initializarea tablei de joc
	Board board;

	// main loop
	while (true)
	{
		system("cls");

		std::cout << "Asa arata tabla de joc:\n";
		std::cout << board << std::endl;

		Piece pickedPiece;
		std::cout << XPlayer << "'s turn";

		Board::Position placedPosition;
		while (true)
		{
			try
			{
				placedPosition = XPlayer.get().PlacePiece(std::cin, std::move(pickedPiece), board);
				//placedPosition = XPlayer.get().MovePiece(std::cin, std::move(pickedPiece), board);
				break;
			}
			catch (const char* errorMessage)
			{
				std::cout << errorMessage << std::endl;
			}
		}
		// conditia de castig
		auto state = BoardStateChecker::Check(board, placedPosition);
		if (state == BoardStateChecker::State::Win)
		{
			std::cout << "Avem un castigator!\nFelicitari: " << XPlayer << std::endl;
		}
		else if (state == BoardStateChecker::State::Draw)
		{
			std::cout << "Joc egal" << std::endl;
		}

		// schimbarea turei celor 2 jucatori
		std::swap(XPlayer, OPlayer);
	}
}