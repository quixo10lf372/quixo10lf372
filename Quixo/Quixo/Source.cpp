#include"../Logging/Logging.h"
#include<iostream>
#include<fstream>
#include"QuixoGame.h"

int main()
{
	// logging
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);
	QuixoGame quixoGame;
	quixoGame.Run();

	return 0;
}