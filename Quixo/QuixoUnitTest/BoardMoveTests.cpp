#include "stdafx.h"
#include "CppUnitTest.h"
#include"Board.h"
#include"Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(PositionIsFalse)
		{
			 Board board;
			 Board::Position position(std::make_pair(200, 200));
			 Board::Position position2(std::make_pair(100, 100));

			 Assert::IsFalse(position == position2);
		}


		TEST_METHOD(PositionZero)
		{
			Board::Position position(std::make_pair(0, 0));
			Board::Position position2(std::make_pair(0, 0));

			Assert::IsTrue(position == position2);
		}


		TEST_METHOD(PositionMinusOne)
		{
			Board::Position position(std::make_pair(-1, -1));
			Board::Position position2(std::make_pair(-1, -1));

			Assert::IsTrue(position == position2);
		}

		TEST_METHOD(LastPosition)
		{
			Board::Position position(std::make_pair(4, 4));
			Board::Position position2(std::make_pair(4, 4));

			Assert::IsTrue(position == position2);
		}



		
	}; // Va multumim pentru vizionare :)
}