#include "stdafx.h"
#include "CppUnitTest.h"
#include"Board.h"
#include"Piece.h"
#include"BoardStateChecker.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(BoardTests)
	{
	public:


		TEST_METHOD(NoneOnePiece)
		{
			Board board;
			Board::Position lastPosition(std::make_pair(200, 200));

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::None);
		}





	};
}