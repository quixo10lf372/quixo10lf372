#include "stdafx.h"
#include "CppUnitTest.h"
#include"Board.h"
#include"Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(BoardTests)
	{
	public:
		// toate testele au trecut
		TEST_METHOD(BoardConstructor)
		{
			Board board;
			Assert::IsFalse(board.isFull());
		}

		TEST_METHOD(BoardException)
		{
			Board board;
			Board::Position position(std::make_pair(200, 200));
			auto boardException = [board, position]() {
				return board[position];
			};
			Assert::ExpectException<const char*>(boardException);
		}

		TEST_METHOD(BoardWithPieces)
		{
			Board board;
			Piece piece(Piece::Face::OFace);
		}

		TEST_METHOD(DefaultConstructorEmptyBoard)
		{
			Board board;
			if (Board::kWidth > 6 && Board::kWidth > 6)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(BoardException1000)
		{
			Board board;
			Board::Position position(std::make_pair(1000, 1000));
			auto boardException = [board, position]() {
				return board[position];
			};
			Assert::ExpectException<const char*>(boardException);
		}




	};
}