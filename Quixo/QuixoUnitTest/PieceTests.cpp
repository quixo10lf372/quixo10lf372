#include "stdafx.h"
#include "CppUnitTest.h"
#include"Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(PieceConstructorWhthO)
		{
			Piece piece(Piece::Face::OFace);
			Assert::IsFalse(piece.GetFace() == Piece::Face::OFace);
		}

		TEST_METHOD(PieceConstructorWithX)
		{
			Piece piece(Piece::Face::XFace);
			Assert::IsFalse(piece.GetFace() == Piece::Face::XFace);
		}

		TEST_METHOD(PieceConstructorWithNone)
		{
			Piece piece(Piece::Face::None);
			Assert::IsTrue(piece.GetFace() == Piece::Face::None);
		}

		TEST_METHOD(ConstructorWithNoParameters)
		{
			Piece piece;
			if (piece.GetFace() == Piece::Face::None )
			{
				
			}
			else
			{
				Assert::Fail();

			}
		}

	};
}