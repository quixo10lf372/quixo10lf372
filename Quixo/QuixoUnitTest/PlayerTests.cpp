#include "stdafx.h"
#include "CppUnitTest.h"
#include"Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(PlayerWithAName)
		{
			const std::string name = "Andrei";
			const char simbol = 'X';
		    Player player( name, simbol);
			Assert::IsTrue(player.getName() == "Andrei");
		}

		TEST_METHOD(PlayerName)
		{
			const std::string name = "Mihai";
			const char simbol = 'O';
			Player player(name, simbol);
			Assert::IsTrue(player.getName() == "Mihai");
		}

		TEST_METHOD(PlayerWithDifferentName)
		{
			const std::string name = "Mihai";
			const char simbol = 'O';
			Player player(name, simbol);
			if (player.getName() == "George")
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(PlayerWithSimbolX)
		{
			const std::string name;
			 char simbol = 'X';
			Player player(name, simbol);
			Assert::IsTrue(player.getSimbol() == 'X');
		}

		TEST_METHOD(PlayerWithSimbolO)
		{
			const std::string name;
			char simbol = 'O';
			Player player(name, simbol);
			Assert::IsTrue(player.getSimbol() == 'O');
		}

		TEST_METHOD(PlayerWithSimbolNone)
		{
			const std::string name;
			char simbol = 'N';
			Player player(name, simbol);
			Assert::IsTrue(player.getSimbol() == 'N');
		}

		TEST_METHOD(PlayerWithDifferentSimbol)
		{
			const std::string name;
			char simbol = 'N';
			Player player(name, simbol);
			if(player.getSimbol() == 'P')
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(PlayerWithDifferentSimbolAndName)
		{
			const std::string name = "Mihai";
			char simbol = 'N';
			Player player(name, simbol);
			if (player.getSimbol() == 'P' && player.getName() == "Andrei")
			{
				Assert::Fail();
			}
		}
	};
}